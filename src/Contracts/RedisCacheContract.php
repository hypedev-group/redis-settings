<?php

namespace HypeDevGroup\RedisSettings\Contracts;

use App\Enums\SettingEnum;

interface RedisCacheContract
{
    public function key(SettingEnum $key): RedisCacheContract;

    public function data($data): RedisCacheContract;

    public function removeCache(): bool;

    public function getCache(): mixed;

    public function refreshCache(): mixed;

    public function cache(): mixed;

    public function get(SettingEnum $key): mixed;
}
