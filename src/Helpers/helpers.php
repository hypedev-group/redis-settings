<?php

use App\Enums\SettingEnum;
use HypeDevGroup\RedisSettings\Facades\RedisCache;

if (!function_exists('setting_get')) {
    function setting_get(SettingEnum $settingEnum, mixed $defaultValue = null): mixed
    {
        $setting = RedisCache::get($settingEnum);

        return $setting ?? $defaultValue;
    }
}