<?php

namespace HypeDevGroup\RedisSettings\Observers;

use HypeDevGroup\RedisSettings\Interfaces\SettingModelInterface;
use HypeDevGroup\RedisSettings\Jobs\ProcessObserver;

class SettingRedisObserver
{
    public function created(SettingModelInterface $model): void
    {
        $this->handler($model, __FUNCTION__);
    }

    public function updated(SettingModelInterface $model): void
    {
        $this->handler($model, __FUNCTION__);
    }

    public function deleted(SettingModelInterface $model): void
    {
        $this->handler($model, __FUNCTION__);
    }

    protected function handler(SettingModelInterface $model, string $action): void
    {
        $handler = new ProcessObserver($model, $action);

        config('settings-redis.observer_events_queue') ? dispatch($handler) : $handler->updated();
    }
}
