<?php

namespace HypeDevGroup\RedisSettings\Cache;

use HypeDevGroup\RedisSettings\Contracts\RedisCacheContract;
use App\Enums\SettingEnum;
use Illuminate\Support\Facades\Redis;

class RedisCache implements RedisCacheContract
{
    protected mixed $data;

    private int $time;

    protected SettingEnum $key;

    public function key(SettingEnum $key): RedisCacheContract
    {
        $this->key = $key;

        return $this;
    }

    public function get(SettingEnum $key): mixed
    {
        $data = Redis::connection(config('redis-settings.redis_connection'))->get($key->value);

        if (!is_null($data))  {
            return $this->unserialize($data);
        }

        return null;
    }

    public function data($data): RedisCacheContract
    {
        $this->data = $data;

        return $this;
    }

    public function getCache(): mixed
    {
        $data = Redis::connection(config('redis-settings.redis_connection'))->get($this->key->value);

        if (!is_null($data))  {
            return $this->unserialize($data);
        }

        return null;
    }

    public function removeCache(): bool
    {
        return Redis::connection(config('redis-settings.redis_connection'))->del($this->key->value);
    }

    public function refreshCache(): mixed
    {
        return $this->key($this->key)->data($this->data)->cache();
    }

    public function cache(): mixed
    {
        Redis::connection(config('redis-settings.redis_connection'))->set($this->key->value, $this->serialize($this->data));

        return $this->data;
    }


    protected function serialize($value): int|string
    {
        if (config('redis-settings.igbinary_serialization')) {
            return igbinary_serialize($value);
        }

        return serialize($value);
    }

    protected function unserialize($value): mixed
    {
        if (config('redis-settings.igbinary_serialization')) {
            return igbinary_unserialize($value);
        }

        return unserialize($value, ['allowed_classes' => true]);
    }
}
