<?php

namespace HypeDevGroup\RedisSettings\Factories;

use App\Enums\SettingEnum;
use Exception;
use HypeDevGroup\RedisSettings\Enums\DataTypeEnum;
use HypeDevGroup\RedisSettings\Models\Setting;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\HypeDevGroup\RedisSettings\Models\Setting>
 */
class SettingFactory extends Factory
{
    protected $model = Setting::class;

    /**
     * @throws Exception
     */
    public function definition(): array
    {
        $dataType = fake()->randomElement(DataTypeEnum::cases());

        while ($dataType->is(DataTypeEnum::FILE)) {
            $dataType = fake()->randomElement(DataTypeEnum::cases());
        }

        return [
            'key' => fake()->unique()->randomElement(SettingEnum::cases())->value,
            'data_type' => $dataType->value,
            'value' => $this->resolveValueByType($dataType),
        ];
    }

    /**
     * @throws Exception
     */
    private function resolveValueByType(DataTypeEnum $type): mixed
    {
        return match ($type) {
            DataTypeEnum::ARRAY => ["test_index" => "test_value", 1 => 3],
            DataTypeEnum::BOOL => fake()->boolean(),
            DataTypeEnum::STRING => fake()->word(),
            DataTypeEnum::INTEGER => fake()->numberBetween(1, 100),
            DataTypeEnum::DATETIME, DataTypeEnum::DATE => Carbon::now()->addHours(random_int(-1000, 1000)),
            DataTypeEnum::FLOAT => fake()->randomFloat(),
            DataTypeEnum::RANGE => ["from" => random_int(1, 99), "to" => random_int(100, 1000)],
            default => throw new Exception("Unsupported data type"),
        };
    }
}
