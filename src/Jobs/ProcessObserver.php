<?php

namespace HypeDevGroup\RedisSettings\Jobs;

use HypeDevGroup\RedisSettings\Interfaces\SettingModelInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use HypeDevGroup\RedisSettings\Facades\RedisCache;

class ProcessObserver implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private SettingModelInterface $model;

    private string $process;


    public function __construct(SettingModelInterface $model, string $process)
    {
        $this->afterCommit = true;
        $this->model = $model;
        $this->process = $process;
    }

    public function handle(): void
    {
        $method = $this->process;

        $this->$method();
    }

    public function created(): void
    {
        RedisCache::key($this->model->getRedisKey())->data($this->model->value)->refreshCache();
    }

    public function deleted(): void
    {
        RedisCache::key($this->model->getRedisKey())->removeCache();
    }

    public function updated(): void
    {
        RedisCache::key($this->model->getRedisKey())->data($this->model->value)->refreshCache();
    }
}
