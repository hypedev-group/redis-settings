<?php

namespace HypeDevGroup\RedisSettings\Models;


use App\Enums\SettingEnum;
use HypeDevGroup\RedisSettings\Enums\DataTypeEnum;
use HypeDevGroup\RedisSettings\Interfaces\SettingModelInterface;
use HypeDevGroup\RedisSettings\Traits\HasDataType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

/**
 * @property SettingEnum $key
 * @property mixed $value
 * @property DataTypeEnum $data_type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Setting extends Model implements SettingModelInterface
{
    use HasFactory;
    use HasDataType;

    protected $fillable = [
        'key',
        'value',
        'data_type'
    ];

    protected $casts = [
        'key' => SettingEnum::class
    ];

    public function getCasts(): array
    {
        if (!Arr::has($this->casts, 'key')) {
            $this->casts['key'] = config('redis-settings.key_cast');
        }

        return array_merge($this->coreCasts, $this->casts);
    }

    public function getRedisKey(): SettingEnum
    {
        return $this->key;
    }

    protected static function newFactory(): \Illuminate\Database\Eloquent\Factories\Factory
    {
        return \HypeDevGroup\RedisSettings\Factories\SettingFactory::new();
    }
}
