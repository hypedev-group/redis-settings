<?php

return [
    /*
    |--------------------------------------------------------------------------
    | RedisSettings Setting Model Observer
    |--------------------------------------------------------------------------
    |
    | This observer class, listening all events on your setting model. Is triggered
    | when you update and delete your setting model.
    |
    |
    */
    'observer' => \HypeDevGroup\RedisSettings\Observers\SettingRedisObserver::class,

    /*
    |--------------------------------------------------------------------------
    | SettingsRedis Connection
    |--------------------------------------------------------------------------
    |
    | This connection is used to store settings data in Redis.
    | You can set specific connection for Redis.
    | If you don't set specific connection, it will use default connection.
    |
    */

    'redis_connection'=> env('REDIS_SETTINGS_REDIS_CONNECTION', 'cache'),

    /*
    |--------------------------------------------------------------------------
    | Is Settings Provider
    |--------------------------------------------------------------------------
    |
    | If this option is true, settings data will be stored in Redis,
    | If this option is false, settings data will be retrieved from Redis, and no
    | observers will be triggered, and no data will be stored in Redis.
    |
    */

    'is_provider'=> env('REDIS_SETTINGS_IS_PROVIDER', false),

    /*
    |--------------------------------------------------------------------------
    | Observer Events Are Queued
    |--------------------------------------------------------------------------
    |
    | If this option is true, model's events are processed as a job on queue.
    | The job will be executed after the database transactions are commit.
    |
    | * ~ Don't forget to run Queue Worker if this option is true. ~ *
    |
    */
    'observer_events_queue' => env('REDIS_SETTINGS_OBSERVER_EVENTS_QUEUE', false),

    /*
    |--------------------------------------------------------------------------
    | Your Settings Model
    |--------------------------------------------------------------------------
    |
    | You can set specific settings model.
    |
    */
    'model' => \HypeDevGroup\RedisSettings\Models\Setting::class,

    /*
    |--------------------------------------------------------------------------
    | Igbinary Serialization
    |--------------------------------------------------------------------------
    |
    | Igbinary Serialization provides a better performance and lower memory
    | usage than PHP Serialization.
    |
    | * ~ Don't forget to enable igbinary extension if this option is true. ~ *
    |
    */
    'igbinary_serialization' => env('JWTREDIS_IGBINARY_SERIALIZATION', false),
];
