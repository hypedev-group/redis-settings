<?php

namespace HypeDevGroup\RedisSettings\Interfaces;

use App\Enums\SettingEnum;

/**
 * @property int $id
 * @property SettingKeysInterface $key
 * @property mixed $value
 */
interface SettingModelInterface
{
    public function getRedisKey(): SettingEnum;
}