<?php

namespace HypeDevGroup\RedisSettings\Facades;

use HypeDevGroup\RedisSettings\Contracts\RedisCacheContract;
use Illuminate\Support\Facades\Facade;

/**
 * Class RedisCache.
 * @method static RedisCacheContract key($getRedisKey)
 */
class RedisCache extends Facade
{
    public static function getFacadeAccessor(): string
    {
        return RedisCacheContract::class;
    }
}
