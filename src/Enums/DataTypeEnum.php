<?php

namespace HypeDevGroup\RedisSettings\Enums;

use HypeDevGroup\RedisSettings\Traits\Equatable;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use JsonException;

enum DataTypeEnum: string
{
    use Equatable;

    case ARRAY = 'array';
    case STRING = 'string';
    case INTEGER = 'integer';
    case DATETIME = 'datetime';
    case DATE = 'date';
    case FLOAT = 'float';
    case BOOL = 'bool';
    case FILE = 'file';
    case RANGE = 'range';

    /**
     * @throws JsonException
     */
    public function castValueForSaving(UploadedFile|array|string $value): ?string
    {
        return match ($this) {
            self::ARRAY, self::RANGE => json_encode($value, JSON_THROW_ON_ERROR),
            self::DATE => Carbon::parse($value)->toDateString(),
            self::DATETIME => Carbon::parse($value)->toDateTimeString(),
            self::BOOL => (bool)$value,
            default => $value
        };
    }

    /**
     * @throws JsonException
     */
    public function getCastedValue(string|array|null $value): mixed
    {
        return match ($this) {
            self::ARRAY, self::RANGE => json_decode($value, true, 512, JSON_THROW_ON_ERROR),
            self::INTEGER => (int)$value,
            self::DATETIME, self::DATE => Carbon::parse($value),
            self::FLOAT => (float)$value,
            self::BOOL => (bool)$value,
            self::FILE => Storage::url($value),
            default => $value,
        };
    }
}
