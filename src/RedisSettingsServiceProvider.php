<?php

namespace HypeDevGroup\RedisSettings;

use HypeDevGroup\RedisSettings\Cache\RedisCache;
use HypeDevGroup\RedisSettings\Contracts\RedisCacheContract;
use Illuminate\Support\ServiceProvider;

class RedisSettingsServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->bindRedisCache();
    }

    public function boot(): void
    {
        $this->publishConfig();
        $this->bindObservers();
    }

    protected function publishConfig(): void
    {
        $this->mergeConfigFrom(__DIR__.'/config/redis-settings.php', 'redis-settings');

        $this->publishes([__DIR__.'/config/redis-settings.php' => config_path('redis-settings.php')], 'config');
        $this->publishes([__DIR__.'/Enums/SettingEnum.php' => app_path('Enums/SettingEnum.php')], 'enums');

        if (config('redis-settings.is_provider')) {
            $this->loadMigrationsFrom(__DIR__.'/migrations');
        }
    }

    protected function bindRedisCache(): void
    {
        $this->app->bind(RedisCacheContract::class, function () {
            return new RedisCache();
        });
    }

    protected function bindObservers(): void
    {
        if (class_exists(config('redis-settings.model')) && config('redis-settings.is_provider')) {
            config('redis-settings.model')::observe(
                config('redis-settings.observer')
            );
        }
    }
}
