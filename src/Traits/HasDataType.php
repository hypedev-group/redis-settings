<?php

namespace HypeDevGroup\RedisSettings\Traits;


use HypeDevGroup\RedisSettings\Enums\DataTypeEnum;
use Illuminate\Database\Eloquent\Casts\Attribute;

/**
 * @property mixed $value
 * @property DataTypeEnum $data_type
 */
trait HasDataType
{
    protected array $coreCasts = [
        'data_type' => DataTypeEnum::class
    ];

    public function value(): Attribute
    {
        return Attribute::make(
            get: fn(mixed $value, array $attributes) => $this->getAttributeValue('data_type')->getCastedValue($value),
            set: static fn(mixed $value, array $attributes) => DataTypeEnum::from(
                $attributes['data_type']
            )->castValueForSaving($value)
        );
    }

    public function getCasts(): array
    {
        return array_merge($this->coreCasts, $this->casts);
    }
}
